import axios from "axios";

const instance = axios.create({
    baseURL: "https://counter-68e2a.firebaseio.com/"
});

export default instance;