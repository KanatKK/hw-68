import React, {useEffect} from 'react';
import './toDo.css';
import {useSelector, useDispatch} from "react-redux";
import {fetchToDo} from "../../store/actions";


const ToDo = () => {
    const dispatch = useDispatch();
    const toDo = useSelector(state => state.toDo);
    const tasks = useSelector(state => state.tasks)
    const toDoList = () => {
        if (toDo !== '') {
            return (<p className={toDo}>{Object.values(toDo) + ''}</p>)
        }
    };

    useEffect(() => {
        dispatch(fetchToDo());
    }, [dispatch]);

    const show = () => {
        console.log(tasks)
    }

    return (
        <div className="container">
            <div className="toDo">
                    <textarea name="toDo" className="areaForTasks" placeholder="ToDo"/>
                    <button onClick={show} type="button" className="addBtn">Add</button>
            </div>
            <h3>To DO:</h3>
            {toDoList()}
        </div>
    );
};

export default ToDo;