import React from 'react';
import './App.css';
import {Route, Switch, BrowserRouter} from 'react-router-dom';
import Counter from "../counter/counter";
import ToDo from "../toDo/toDo";
import Switcher from "../../components/switcher/switcher";

const App = () => {
  return (
    <BrowserRouter>
        <Route component={Switcher}/>
        <Switch>
            <Route path="/" exact component={Counter}/>
            <Route path="/toDo" exact component={ToDo}/>
        </Switch>
    </BrowserRouter>
  )
}

export default App;
