import React, {useEffect} from "react";
import {useSelector, useDispatch} from "react-redux";
import "./counter.css";
import {
    addCounter,
    decrementCounter,
    fetchCounter,
    incrementCounter,
    subtractCounter,
    activateBtn,
} from "../../store/actions";
import axios from "../../axios-api";

const Counter = () => {
    const counter = useSelector(state => state.counter);
    const disabled = useSelector(state => state.disabled);
    const dispatch = useDispatch();

    const incrementCounterHandler = async (value) => {
        await dispatch(incrementCounter(value));
        await axios.put("/counter.json", Number(counter + value));
        await dispatch(activateBtn());
    };
    const decrementCounterHandler = async (value) => {
        await dispatch(decrementCounter(value));
        await axios.put("/counter.json", Number(counter - value));
        await dispatch(activateBtn());
    };
    const addCounterHandler = async (value) => {
        await dispatch(addCounter(value));
        await axios.put("/counter.json", Number(counter + value));
        await dispatch(activateBtn());
    };
    const subtractCounterHandler = async (value) => {
        await dispatch(subtractCounter(value));
        await axios.put("/counter.json", Number(counter - value));
        await dispatch(activateBtn());
    };

    useEffect(() => {
        dispatch(fetchCounter());
    }, [dispatch]);

    return (
        <div className="container">
            <div className="Counter">
                <h1>{counter}</h1>
                <button disabled={disabled} onClick={() => incrementCounterHandler(1)}>Increase</button>
                <button disabled={disabled} onClick={() => decrementCounterHandler(1)}>Decrease</button>
                <button disabled={disabled} onClick={() => addCounterHandler(5)}>Increase by 5</button>
                <button disabled={disabled} onClick={() => subtractCounterHandler(5)}>Decrease by 5</button>
            </div>
        </div>
    );
};

export default Counter;