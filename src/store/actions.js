import axios from "../axios-api";
import {
    ADD,
    ACTIVATE,
    DECREMENT,
    FETCH_COUNTER_ERROR,
    FETCH_COUNTER_REQUEST,
    FETCH_COUNTER_SUCCESS,
    INCREMENT,
    SUBTRACT, FETCH_TODO_REQUEST, FETCH_TODO_SUCCESS, FETCH_TODO_ERROR, GET_ACTIONS,
} from "./actionTypes";

export const incrementCounter = value => {
    return {type: INCREMENT, value};
};
export const decrementCounter = value => {
    return {type: DECREMENT, value};
};
export const addCounter = value => {
    return {type: ADD, value};
};
export const subtractCounter = value => {
    return {type: SUBTRACT, value};
};
export const activateBtn = () => {
    return {type: ACTIVATE};
};

const fetchCounterRequest = () => {
    return {type: FETCH_COUNTER_REQUEST};
};
const fetchCounterSuccess = value => {
    return {type: FETCH_COUNTER_SUCCESS, value};
};
const fetchCounterError = error => {
    return {type: FETCH_COUNTER_ERROR, error};
};

export const fetchCounter = () => {
    return async dispatch => {
        dispatch(fetchCounterRequest());
        try {
            const response = await axios.get("/counter.json");
            dispatch(fetchCounterSuccess(response.data));
        } catch(e) {
            dispatch(fetchCounterError(e));
        }
    };
};

export const fetchToDoRequest = () => {
    return {type: FETCH_TODO_REQUEST};
};
export const fetchToDoSuccess = value => {
    return {type: FETCH_TODO_SUCCESS, value};
};
export const fetchToDoError = error => {
    return {type: FETCH_TODO_ERROR, error};
};

export const fetchToDo = () => {
    return async dispatch => {
        dispatch(fetchToDoRequest());
        try {
            const response = await axios.get("/toDo.json");
            dispatch (fetchToDoSuccess(response.data));
        } catch (e) {
            dispatch(fetchToDoError());
        }
    };
};
