import {
    ACTIVATE,
    ADD,
    DECREMENT,
    FETCH_COUNTER_ERROR,
    FETCH_COUNTER_REQUEST,
    FETCH_COUNTER_SUCCESS,
    INCREMENT,
    SUBTRACT,
    FETCH_TODO_ERROR,
    FETCH_TODO_REQUEST,
    FETCH_TODO_SUCCESS,
} from "./actionTypes";

const initialState = {
    counter: 0,
    loading: false,
    error: null,
    disabled: false,
    toDo: '',
    tasks: '',
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case INCREMENT:
            return {...state, counter: state.counter + action.value, disabled: true};
        case DECREMENT:
            return {...state, counter: state.counter - action.value, disabled: true};
        case ADD:
            return {...state, counter: state.counter + action.value, disabled: true};
        case SUBTRACT:
            return {...state, counter: state.counter - action.value, disabled: true};
        case FETCH_COUNTER_REQUEST:
            return {...state, loading: true};
        case FETCH_COUNTER_SUCCESS:
            return {...state, loading: false, counter: action.value};
        case FETCH_COUNTER_ERROR:
            return {...state, loading: false, error: action.error};
        case FETCH_TODO_REQUEST:
            return {...state, loading: true};
        case FETCH_TODO_SUCCESS:
            return {...state,loading: false, toDo: action.value};
        case FETCH_TODO_ERROR:
            return {...state, loading: false, error: action.error};
        case ACTIVATE:
            return {...state, disabled: false};
        default:
            return state;
    }
};

export default reducer;