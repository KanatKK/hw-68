import React from 'react';
import './switcher.css';

const Switcher = props => {
    const counterSwitcher = () => {
        props.history.push('/');
    };

    const toDoSwitcher = () => {
        props.history.push('/toDo');
    };

    return (
        <div className="switcher">
            <button type="button" onClick={counterSwitcher}>Counter</button>
            <button type="button" onClick={toDoSwitcher}>To Do</button>
        </div>
    );
};

export default Switcher;